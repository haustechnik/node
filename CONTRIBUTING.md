# Contributing

## Branching Model

In diesem Repository wird parallel Hardware (Elektronik) und Software (für die Elektronik) entwickelt.
Die Software ist von der Elektronik abhängig, die Elektronikentwicklung sollte auf die Softwareentwickling "Rücksicht nehmen".
Damit diese parallele Entwicklung trotz der stark unterschiedlichen Zykluszeiten (einige Monate für die Hardware, einige Tage für die Software) möglich ist wird der folgende Workflow verwendet.

### Hardware-Branches werden möglichst nicht zusammengeführt

Die Entwicklung läuft primär im `hardware`-Branch.
Jede bestellte Platine ist eine neue _Major Revision_ und wird mit `vX.0-hardware` getaggt.
Dieser Tag stellt den Ursprung für den Patch-Branch `hw-revX` dar, in welchem Änderungen an der gefertigten Platine dokumentiert werden.
Parallel dazu läuft in `hardware` die Entwicklung für den nächsten Prototypen / das nächste Release.

Die meisten KiCad-Dateien lassen sich nicht wie reine Textdateien automatisch aus verschiedenen Entwicklungszweigen zusammenführen, sodass dies von vornherein vermieden werden sollte.
Darüber hinaus werden hier die meisten KiCad-Quelldateien ohnehin von _Git LFS_ verwaltet, was ein nachträgliches _mergen_ zusätzlich erschwert.
Häufig ist es schneller, die notwendigen Änderungen manuell zu übertragen oder mit [git-rebase](https://git-scm.com/docs/git-rebase) einzupflegen.

### Software-Branches orientiert am Gitflow-Workflow

Vincent Driesen hat [in seinem Blog einen Git-Workflow vorgestellt](https://nvie.com/posts/a-successful-git-branching-model/), welcher sich im Nachhinein unter dem Namen _Gitflow_ durchgesetzt hat.
An dieses Schema angelehnt soll hier die Softwareentwicklung laufen.

Der `software`-Branch spiegelt den hauptsächlichen Entwicklungszweig wieder und entspricht dem [`develop`](https://nvie.com/posts/a-successful-git-branching-model/#the-main-branches)-Branch im originalen _Gitflow_-Modell.
Nach einer neuen _Major Revision_ `vY.0-hardware` in der Hardware wird dieser Stand in den `software`-Branch ge*merged*, ab welchem dieser für die Entwicklung der Software für diese Hardware-Revision gedacht ist.
Sollten nach diesem Zeitpunkt noch Änderungen an der Software für den vorherigen Stand der Hardware gemacht werden, ist hierfür ein Patch-Branch `sw-revY` zu eröffnen.
In `hw-revX` dokumentierte Änderungen sind diagonal in den `software`-Branch bzw. nach `sw-revX` zu *mergen*, damit dort die jeweils korrekten Schaltplan-Versionen vorliegen.

Die Verfahrensweise mit dem `software`-Branch läuft wie im originalen Gitflow-Modell ab.
Unabhängige Features werden in [_Feature Branches_](https://nvie.com/posts/a-successful-git-branching-model/#feature-branches) entwickelt,
neue Releases in [_Release Branches_](https://nvie.com/posts/a-successful-git-branching-model/#release-branches) vorbereitet
und (sofern erforderlich) für Hotfixes entsprechende [_Hotfix Branches_](https://nvie.com/posts/a-successful-git-branching-model/#hotfix-branches) angelegt.

## Commit Messages

[In einem Artikel auf seiner Website](https://chris.beams.io/posts/git-commit/) erklärt Chris Beams, was eine gute _Commit Message_ ausmacht und warum das wichtig ist.
_Commit Messages_ sind in Englisch zu halten, und die [sieben Regeln](https://chris.beams.io/posts/git-commit/#seven-rules) helfen gewaltig dabei, gute Nachrichten zu schreiben.
