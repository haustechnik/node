# Platinenhalter (Hutschienenmontage)

## Voelkner

| Beschreibung  | Länge     | Produktnummer  | Preis  |
| ------------- | ---------:| -------------- | ------:|
| Hauptelement  | 45 mm     | CIME/M/BE4500  | 1.43 € |
| Hauptelement  | 22.5 mm   | CIME/M/BE2250  | 0.72 € |
| Hauptelement  | 11.25 mm  | CIME/M/BE1125  | 0.51 € |
| Endteil (Fuß) | 22.5 mm   | CIME/M/SEF2250 | 1.27 € |
| Endteil (Fuß) | 11.25 mm  | CIME/M/SEF1125 | 0.80 € |
