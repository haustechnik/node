#ifndef __ht_createTarget_H
#define __ht_createTarget_H

#include "ht/ht-main.h"

void CreateTarget_StartTask(void);
void CreateTarget_StopTask(void);

#endif /* __ht_createTarget_H */