#ifndef __ht_di_H
#define __ht_di_H

#include "ht/ht-main.h"

void DI_InitTask(void);
void DI_StartTask(void);
void DI_StopTask(void);

void DI_InterruptCallback(uint16_t pin);

QueueHandle_t DITransmitQueueHandle;

typedef enum {
    diLevelHigh,
    diLevelLow,
    diActionClick,
    diActionDClick
} diLevelOrAction;

typedef struct {
    uint8_t ind;
    diLevelOrAction what;
} diTransmitQueueEntry_t;

#endif /* __ht_di_H */