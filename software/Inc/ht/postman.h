#ifndef __ht_postman_H
#define __ht_postman_H

#include "ht/ht-main.h"

void postman_StartTask(void);
void postman_StopTask(void);
void postman_InitTask(void);

#endif /* __ht_postman_H */