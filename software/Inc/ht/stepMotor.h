#ifndef __ht_stepMotor_H
#define __ht_stepMotor_H

#include "ht/ht-main.h"

void StepMotor_StartTask(void);
void StepMotor_StopTask(void);

#endif /* __ht_stepMotor_H */