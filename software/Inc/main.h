/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "stm32l0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
#define len(arr) ((sizeof(arr) / sizeof(arr[0])))
/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define PS_SLEEP_Pin GPIO_PIN_13
#define PS_SLEEP_GPIO_Port GPIOC
#define AI1_Pin GPIO_PIN_0
#define AI1_GPIO_Port GPIOC
#define AI2_Pin GPIO_PIN_1
#define AI2_GPIO_Port GPIOC
#define AI3_Pin GPIO_PIN_2
#define AI3_GPIO_Port GPIOC
#define AI4_Pin GPIO_PIN_3
#define AI4_GPIO_Port GPIOC
#define AO1_Pin GPIO_PIN_0
#define AO1_GPIO_Port GPIOA
#define AO2_Pin GPIO_PIN_1
#define AO2_GPIO_Port GPIOA
#define AO3_Pin GPIO_PIN_2
#define AO3_GPIO_Port GPIOA
#define AO4_Pin GPIO_PIN_3
#define AO4_GPIO_Port GPIOA
#define RI_Pin GPIO_PIN_4
#define RI_GPIO_Port GPIOA
#define RIX_Pin GPIO_PIN_5
#define RIX_GPIO_Port GPIOA
#define VREF_EN_Pin GPIO_PIN_7
#define VREF_EN_GPIO_Port GPIOA
#define amux_nEN_Pin GPIO_PIN_5
#define amux_nEN_GPIO_Port GPIOC
#define amux_a0_Pin GPIO_PIN_0
#define amux_a0_GPIO_Port GPIOB
#define amux_a1_Pin GPIO_PIN_1
#define amux_a1_GPIO_Port GPIOB
#define amux_a2_Pin GPIO_PIN_2
#define amux_a2_GPIO_Port GPIOB
#define RS485_TX_Pin GPIO_PIN_10
#define RS485_TX_GPIO_Port GPIOB
#define RS485_RX_Pin GPIO_PIN_11
#define RS485_RX_GPIO_Port GPIOB
#define RS485_DE_Pin GPIO_PIN_12
#define RS485_DE_GPIO_Port GPIOB
#define addr0_Pin GPIO_PIN_13
#define addr0_GPIO_Port GPIOB
#define addr1_Pin GPIO_PIN_14
#define addr1_GPIO_Port GPIOB
#define addr2_Pin GPIO_PIN_15
#define addr2_GPIO_Port GPIOB
#define addr3_Pin GPIO_PIN_6
#define addr3_GPIO_Port GPIOC
#define addr4_Pin GPIO_PIN_7
#define addr4_GPIO_Port GPIOC
#define addr5_Pin GPIO_PIN_8
#define addr5_GPIO_Port GPIOC
#define addr6_Pin GPIO_PIN_9
#define addr6_GPIO_Port GPIOC
#define DI1_Pin GPIO_PIN_8
#define DI1_GPIO_Port GPIOA
#define DI1_EXTI_IRQn EXTI4_15_IRQn
#define DI2_Pin GPIO_PIN_9
#define DI2_GPIO_Port GPIOA
#define DI2_EXTI_IRQn EXTI4_15_IRQn
#define DI3_Pin GPIO_PIN_10
#define DI3_GPIO_Port GPIOA
#define DI3_EXTI_IRQn EXTI4_15_IRQn
#define DI4_Pin GPIO_PIN_11
#define DI4_GPIO_Port GPIOA
#define DI4_EXTI_IRQn EXTI4_15_IRQn
#define DI5_Pin GPIO_PIN_12
#define DI5_GPIO_Port GPIOA
#define DI5_EXTI_IRQn EXTI4_15_IRQn
#define DI6_Pin GPIO_PIN_15
#define DI6_GPIO_Port GPIOA
#define DI6_EXTI_IRQn EXTI4_15_IRQn
#define DI7_Pin GPIO_PIN_10
#define DI7_GPIO_Port GPIOC
#define DI8_Pin GPIO_PIN_11
#define DI8_GPIO_Port GPIOC
#define DO1_Pin GPIO_PIN_2
#define DO1_GPIO_Port GPIOD
#define DO2_Pin GPIO_PIN_3
#define DO2_GPIO_Port GPIOB
#define DO3_Pin GPIO_PIN_4
#define DO3_GPIO_Port GPIOB
#define DO4_Pin GPIO_PIN_5
#define DO4_GPIO_Port GPIOB
#define DO5_Pin GPIO_PIN_6
#define DO5_GPIO_Port GPIOB
#define DO6_Pin GPIO_PIN_7
#define DO6_GPIO_Port GPIOB
#define DO7_Pin GPIO_PIN_8
#define DO7_GPIO_Port GPIOB
#define DO8_Pin GPIO_PIN_9
#define DO8_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
uint16_t adcBuffer[7];
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
