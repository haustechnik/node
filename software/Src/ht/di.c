#include "ht/di.h"

TaskHandle_t DITaskHandle;
QueueHandle_t DIActionQueueHandle;
TimerHandle_t clickDetectTimer[6];

typedef struct diInterruptContext
{
    uint8_t ind : 4;
    uint8_t risingEdge : 1;
    uint8_t fallingEdge : 1;
    uint8_t timeout : 1;
} diInterruptContext_t;


const portPin_t diMapping[] = {
    {DI1_GPIO_Port, DI1_Pin},
    {DI2_GPIO_Port, DI2_Pin},
    {DI3_GPIO_Port, DI3_Pin},
    {DI4_GPIO_Port, DI4_Pin},
    {DI5_GPIO_Port, DI5_Pin},
    {DI6_GPIO_Port, DI6_Pin}
};

uint32_t timerDuration[6] = {500, 500, 250, 5, 5, 5};
uint8_t timerId[6] = {0, 1, 2, 3, 4, 5};

typedef enum {
    diStateOff, 
    diStateWait,
    diStateClickWait,
    diStateDClickWait,
    diStateOn,
    diStateClick,
    diStateDClick,
    diStateErr
} diState;
diState diStateMapping[6] = {diStateErr};

typedef enum {diTransitionRising, diTransitionFalling, diTransitionTimeOut} diStateTransition;

void DI_Task(void *param);
void DI_TimerCallBack(TimerHandle_t tHandle);
diState DI_StateMachineAdvance(diState actState, diStateTransition action);


void DI_StartTask(void) {
    startTask(&DITaskHandle, DI_Task, "DI Handler", 64, 4);
}

void DI_InitTask(void) {
    if (DIActionQueueHandle == NULL) {
        DIActionQueueHandle = xQueueCreate(10, sizeof(diInterruptContext_t));
    }

    for (uint8_t i = 0; i < len(clickDetectTimer); i++) {
        if (clickDetectTimer[i] == NULL)
            clickDetectTimer[i] = xTimerCreate("timer", pdMS_TO_TICKS(timerDuration[i]), pdFALSE, &timerId[i], DI_TimerCallBack);
    }

    for (uint8_t i = 0; i < len(diStateMapping); i++) {
        GPIO_PinState actState = HAL_GPIO_ReadPin(diMapping[i].port, diMapping[i].pin);
        diStateMapping[i] = actState == GPIO_PIN_SET ? diStateOn : diStateOff;
        diTransmitQueueEntry_t txEntry = {.ind = i, .what = actState == GPIO_PIN_SET ? diLevelHigh : diLevelLow};
        xQueueSend(DITransmitQueueHandle, &txEntry, 0);
    }

}

void DI_StopTask(void) {
    stopTask(&DITaskHandle);
}

void DI_Task(void *param) {

    diInterruptContext_t actInterrupt;

    while (true) {
        // wait indefinitely for queue
        xQueueReceive(DIActionQueueHandle, &actInterrupt, portMAX_DELAY);
        
        diStateTransition action;
        if (actInterrupt.risingEdge) action = diTransitionRising;
        else if (actInterrupt.fallingEdge) action = diTransitionFalling;
        else if (actInterrupt.timeout) action = diTransitionTimeOut;
        else continue;

        uint8_t actInd = actInterrupt.ind;

        const portPin_t *currentDI = &diMapping[actInd];
        diState *currentState = &diStateMapping[actInd];

        *currentState = DI_StateMachineAdvance(*currentState, action);

        diTransmitQueueEntry_t txEntry = {.ind = actInd};

        if (*currentState == diStateWait) {
            if (xTimerStart(clickDetectTimer[actInd], 0) != pdPASS)
                *currentState = diStateErr;
        }
 
        switch (*currentState) {
            case diStateClick: 
                txEntry.what = diActionClick;
                xQueueSend(DITransmitQueueHandle, &txEntry, 0);
                *currentState = diStateOff;
                break;
            case diStateDClick: // Nachricht bereitstellen, timer stoppen, nach Off weitergehen
                xTimerStop(clickDetectTimer[actInd],0);
                txEntry.what = diActionDClick;
                xQueueSend(DITransmitQueueHandle, &txEntry, 0);
                *currentState = diStateOff;
                break;
            case diStateErr:   // timer stoppen, manuell on oder off weitergehen
                xTimerStop(clickDetectTimer[actInd],0);
                GPIO_PinState actLevel = HAL_GPIO_ReadPin(currentDI->port, currentDI->pin);
                *currentState = actLevel == GPIO_PIN_SET ? diStateOn : diStateOff;
                break;            
            default:
                break;
        }

        switch (*currentState) {
            case diStateOn: // Nachricht bereitstellen
                txEntry.what = diLevelHigh;
                xQueueSend(DITransmitQueueHandle, &txEntry, 0);
                break;
            case diStateOff: // Nachricht bereitstellen
                txEntry.what = diLevelLow;
                xQueueSend(DITransmitQueueHandle, &txEntry, 0);
                break;
            default:
                break;
        }
    }
}

/**
 * @brief Handle DI interrupt.
 * 
 * In case multiple interrupts should happen at once, this function is being
 * called for every pin separately.
 * 
 * Unfortunately this only works for DI1 through DI6.
 * DI7 and DI8 are currently NOT supported.
 * 
 * @param pin Interrupt source pin. Only a single bit is set.
 */
void DI_InterruptCallback(uint16_t pin) {
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    bool risingEdge;

    if (DITaskHandle != NULL) {
        
        int8_t ind = -1;

        switch (pin) {
            case DI1_Pin: ind = 0; break;
            case DI2_Pin: ind = 1; break;
            case DI3_Pin: ind = 2; break;
            case DI4_Pin: ind = 3; break;
            case DI5_Pin: ind = 4; break;
            case DI6_Pin: ind = 5; break;
        }

        if (ind >= 0) {
            risingEdge = HAL_GPIO_ReadPin(diMapping[ind].port, diMapping[ind].pin) == GPIO_PIN_SET;
            diInterruptContext_t actDi = {
                .ind = ind, 
                .risingEdge = risingEdge, 
                .fallingEdge = !risingEdge,
                .timeout = 0};
            xQueueSendToBackFromISR(DIActionQueueHandle, &actDi, &xHigherPriorityTaskWoken);
        }

    }

    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void DI_TimerCallBack(TimerHandle_t tHandle) {
    diInterruptContext_t timerContext = {0};
    timerContext.timeout = 1;
    timerContext.ind = * (uint8_t*)pvTimerGetTimerID(tHandle);
    xQueueSend(DIActionQueueHandle, &timerContext, 0);
}

diState DI_StateMachineAdvance(diState actState, diStateTransition action) {
    switch (actState) {
        case diStateOff:
            switch (action) {
                case diTransitionRising:  return diStateWait;
                case diTransitionFalling: return diStateErr;
                case diTransitionTimeOut: return diStateOff;
            }
        case diStateWait:
            switch (action) {
                case diTransitionRising:  return diStateErr;
                case diTransitionFalling: return diStateClickWait;
                case diTransitionTimeOut: return diStateOn;
            }
        case diStateClickWait:
            switch (action) {
                case diTransitionRising:  return diStateDClickWait;
                case diTransitionFalling: return diStateErr;
                case diTransitionTimeOut: return diStateClick;
            }
        case diStateDClickWait:
            switch (action) {
                case diTransitionRising:  return diStateErr;
                case diTransitionFalling: return diStateDClick;
                case diTransitionTimeOut: return diStateOn;
            }
        case diStateOn:
            switch (action) {
                case diTransitionFalling: return diStateOff;
                default:                  return diStateErr;
            }
        case diStateClick:                return diStateOff;
        case diStateDClick:               return diStateOff;
        default:                          return diStateErr;
    
    }
}

