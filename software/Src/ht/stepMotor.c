#include "ht/di.h"
#include "ht/emil.h"

TaskHandle_t StepMotorTaskHandle;

void StepMotorTask(void *args);
double calcDuration();
void StepMotor_MotorTimerCallBack(TimerHandle_t tHandle);
void StepMotor_PeriodicTimerCallBack(TimerHandle_t tHandle);

const portPin_t doMapping[] = {
    {DO1_GPIO_Port, DO1_Pin},
    {DO2_GPIO_Port, DO2_Pin},
    {DO3_GPIO_Port, DO3_Pin},
    {DO4_GPIO_Port, DO4_Pin},
    {DO5_GPIO_Port, DO5_Pin},
    {DO6_GPIO_Port, DO6_Pin}
};

//const portPin_t *white  = &doMapping[0];
const portPin_t *red    = &doMapping[1];
const portPin_t *yellow = &doMapping[2];
//const portPin_t *green  = &doMapping[3];
const portPin_t *blue   = &doMapping[4];

typedef enum {
    unknown,
    up,
    down
    } DODirection;
DODirection lastMove = unknown;

uint8_t supposedValue = 0;
const uint8_t deadBand = 2;
const uint8_t backlash = 10;
const uint8_t duration = 60;
/*const uint8_t synchTimeFirstInMinutes = 1;
const uint8_t synchTimeLastInMinutes = 5;
const uint8_t synchDist = 10;
*/

bool timerRunning = false;
uint8_t stepMotorTimerId = 1;

void StepMotor_StartTask(void) {
    startTask(&StepMotorTaskHandle, StepMotorTask, "DI Handler", 64, 2);
}

void StepMotor_StopTask(void) {
    stopTask(&StepMotorTaskHandle);
}

void StepMotorTask(void *args) {

/*    TimerHandle_t periodic = xTimerCreate("timer", pdMS_TO_TICKS(10000), pdTRUE, &stepMotorTimerId, StepMotor_PeriodicTimerCallBack);
    BaseType_t timerResult = xTimerStart(periodic, 0);
    if (timerResult != pdPASS) {
        Error_Handler();
    }
*/
    while (true) {
        StepMotor_PeriodicTimerCallBack(NULL);
        vTaskDelay(pdMS_TO_TICKS(10000));
    }
}

void StepMotor_PeriodicTimerCallBack(TimerHandle_t tHandle) {
    if (!timerRunning) {
        double actDuration = calcDuration();
        if (actDuration > 0) {
            if (DOTargetValue < supposedValue 
             || ((DOTargetValue == DORangeLow) && DOSynch)) {
                lastMove = down;
                HAL_GPIO_WritePin(blue->port, blue->pin, GPIO_PIN_SET);
                stepMotorTimerId = 0;
            } else {
                lastMove = up;
                HAL_GPIO_WritePin(yellow->port, yellow->pin, GPIO_PIN_SET);
                stepMotorTimerId = 1;
            }
            TimerHandle_t term = xTimerCreate("timer", pdMS_TO_TICKS((uint32_t)(actDuration * 1000)), pdFALSE, &stepMotorTimerId, StepMotor_MotorTimerCallBack);
            xTimerStart(term, 0);
            timerRunning = true;

            supposedValue = DOTargetValue;
            DOSynch = false;        
        }
    }
}

double calcDuration() {
    const double durationPerStep = ((double)duration)/(DORangeHigh-DORangeLow);
    double actDuration = 0;
    if (DOSynch)
        actDuration = 1.5 * duration;
    else {
        int16_t diff = DOTargetValue - supposedValue;
        if (diff < 0) 
            diff *= (-1);
        if (diff > deadBand) {
            double offset = 0;
            if (((DOTargetValue > supposedValue) && lastMove == down) ||
                ((DOTargetValue < supposedValue) && lastMove == up))
                offset = backlash;
            else if (lastMove == unknown)
                offset = ((double)backlash)/2;
            actDuration = durationPerStep * (offset + diff);
        }
    }
    return actDuration;
}

void StepMotor_MotorTimerCallBack(TimerHandle_t tHandle) {
    uint8_t actTimer = * (uint8_t*)pvTimerGetTimerID(tHandle);
    if (actTimer == 0)
        HAL_GPIO_WritePin(blue->port, blue->pin, GPIO_PIN_RESET);
    else
        HAL_GPIO_WritePin(yellow->port, yellow->pin, GPIO_PIN_RESET);

    timerRunning = false;
}